# 例子1： 一个简单的猜数字小游戏，用户输入数字，程序告诉是大了还是小了，或者猜中。
"""
例子1： 一个简单的猜数字小游戏。
    用户输入数字，程序告诉是大了还是小了，或者猜中。
"""
# 引入random模块，随机模块
import random
# 调用randint方法获取1-100内的整数
randomNum = random.randint(1,100)
# again表示程序继续执行，用户继续猜数字
again = True
while again:
    number = input("您猜的数字是：")
    number = int(number)
    if number < randomNum:
        print("您输入的数字小了!")
    elif number > randomNum:
        print("您输入的数字大了!")
    else:
        # 猜中了，设置again为false，跳出下一次循环
        again = False
        print("恭喜您，猜中了!")

print("程序结束")




