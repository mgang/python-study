# python-study

## 介绍
和妙一起学python

## 快速安装包
```
pip install 安装包名 -i https://pypi.tuna.tsinghua.edu.cn/simple
```

## 学习进度
[具体笔记参考processn思维导图](https://www.processon.com/mindmap/5f409a421e085306e16e9acc
)
* 20200822第一课 - 妙突然说要学python?大刚来确认，并介绍计算机相关知识
* 20200828第二课 - python介绍和环境安装，IDE介绍以及第一个程序[hello world.py](./basic/helloWorld.py)
* 20200913第三课 - python基础第一讲，变量，运算符，循环控制，保留字，及猜数字小游戏程序[guessNumber.py](./basic/guessNumber.py)

......未完待续

## 目录结构
* basic - 基础，包含语法，控制循环等；以及helloworld和guessNumber小游戏。
* advance - 进阶，包含数组，元组，列表，字典，模块，函数，文件IO，异常处理等。
* demo - 主要包含一些例子，具体如下：
> gitee-pages 基于requests来脚本刷新gitee pages
> barspeed 调整图竞赛
> datashow http://pys.rasp.meiflower.top:81/view/demo/datashow/ds.html 数据统计的小例子
