;MOV A, 3; 将3存入A寄存器
;NOP
;ADD A, 20;
;MOV D, A;
;MOV C,[2];

;MOV A,[2];
;MOV C,[A];
;MOV D,100;
;MOV [0x22],2;
;MOV [0x36],[0x22];
;MOV [0x33],D;

;MOV C,22;
;;MOV [C],[2];ram c位置的值=ram 9位置的值
;MOV D,100;
;MOV [C],D;
;MOV [D],4;

;MOV D, 0;

;INC D;
;INC D;
;INC D;
;INC D;
;INC D;
;
;DEC D;
;DEC D;
;DEC D;
;DEC D;
;DEC D;

;MOV D,1;
;MOV C,2;
;SUB D,C;
;SUB D,8;

;AND D,C;
;OR D,C;
;XOR D,C;

;or D,0x11;

;not d;

;MOV D,1;
;aaa:
;    INC D
;    CMP D,5
;    JO aaa
;bbb:
;    DEC D
;    CMP D,0
;    JZ aaa
;    JNO bbb
;; 停止
;HLT;

;; 栈段初始值为1，则内存高位地址为1
;mov SS, 1
;; 栈指针初始位置16，则下一个栈顶为15，会放到RAM的Ox010f位置
;MOV SP, 0x10 ; [0, 0xf]
;mov D, 5;
;
;FF:
;    push D
;    DEC D
;    CMP D,0
;    JZ EE
;    JNO FF
;
;EE:
;    pop D
;    CMP D,5
;    JZ ET
;    JO EE
;
;ET:
;    HLT

    mov ss, 1
    MOV SP, 0x20 ; [0, 0xf]
    jmp start

show:
    mov d, 255;
    iret; return;

start:
    mov c, 0

increase:
    inc c;
    mov d, c;
    JP disable
enable:
    sti;
    jmp interrupt
disable:
    cli

interrupt:
    int show
    jmp increase

    HLT


