import pyautogui
import time
from selenium import webdriver
import selenium.webdriver.support.ui as ui

browser = webdriver.Chrome()
# 需要设置浏览器的window.navigator.webdriver=undefined，不然无法通过滑块检查（速卖通加了webdriver禁止）
# https://blog.csdn.net/weixin_43881394/article/details/108467118?spm=1005.2026.3001.5635&utm_medium=distribute.pc_relevant_ask_down.none-task-blog-2~default~OPENSEARCH~Rate-5.pc_feed_download_top3ask&depth_1-utm_source=distribute.pc_relevant_ask_down.none-task-blog-2~default~OPENSEARCH~Rate-5.pc_feed_download_top3ask
browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
    "source": """Object.defineProperty(navigator, 'webdriver', {get: () => undefined})""",
})
# 最大化
browser.maximize_window()
# 设置cookies跳过登录
cookies = [{'domain': '.aliexpress.com', 'expiry': 1664775707, 'httpOnly': False, 'name': 'tfstk', 'path': '/', 'secure': False, 'value': 'czbhBAjg-dMX7J-GcyTC_fbJb2yOZADJngSG7ZHVzmxdzgQNigmZ0MQ0IpwbL01..'}, {'domain': '.aliexpress.com', 'expiry': 1664775707, 'httpOnly': False, 'name': 'l', 'path': '/', 'secure': False, 'value': 'eBr_a05PLRFadef6BOfZnurza779sIRfguPzaNbMiOCPOvCe53_vX6VQVtYwCnGVnsZH23lXiuzUB4YiOy49hk-DS9wdVEbr3d6G.'}, {'domain': '.aliexpress.com', 'expiry': 1649310107, 'httpOnly': False, 'name': 'xlly_s', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': '1'}, {'domain': '.aliexpress.com', 'httpOnly': False, 'name': 'ali_apache_tracktmp', 'path': '/', 'secure': False, 'value': ''}, {'domain': '.aliexpress.com', 'expiry': 1664775699, 'httpOnly': False, 'name': 'isg', 'path': '/', 'secure': False, 'value': 'BOnpwYPBr1WfRJPh0508Aw-t-JNDtt3oO2hUIYveb1APUghkygWlubxEEPbkSnUg'}, {'domain': '.aliexpress.com', 'expiry': 1964583698, 'httpOnly': False, 'name': 'e_id', 'path': '/', 'secure': False, 'value': 'pt50'}, {'domain': '.aliexpress.com', 'expiry': 3796707356, 'httpOnly': False, 'name': 'ali_apache_track', 'path': '/', 'secure': False, 'value': ''}, {'domain': '.aliexpress.com', 'expiry': 2279943699, 'httpOnly': False, 'name': 'cna', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': 'BRbUGsx4PVYCASv13rHbVOJG'}, {'domain': '.aliexpress.com', 'httpOnly': False, 'name': 'acs_usuc_t', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': 'acs_rt=6458a8bd798848d8b670161c766b3c02&x_csrf=13u0vlqvb6lmm'}, {'domain': '.aliexpress.com', 'expiry': 3796707356, 'httpOnly': False, 'name': 'xman_us_f', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': 'x_l=0&acs_rt=6458a8bd798848d8b670161c766b3c02'}, {'domain': '.aliexpress.com', 'expiry': 3796707346, 'httpOnly': True, 'name': 'xman_f', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': 'Qfmft1w5iLeoTOWTRxfYBi38dAgOwyh2iWDVBNe0+b/10YEsHp4xCW491IqQg4eSwgUl0UcGfP8RshPKx5KkRLLeVBezwyC9AFO1EFBVz8wnFIM7kgwAUQ=='}, {'domain': '.aliexpress.com', 'expiry': 3626384476, 'httpOnly': False, 'name': 'ali_apache_id', 'path': '/', 'secure': False, 'value': '33.0.187.73.1649223684710.646206.4'}, {'domain': 'login.aliexpress.com', 'expiry': 1664775699, 'httpOnly': False, 'name': '_bl_uid', 'path': '/', 'secure': False, 'value': 'avlCe1whn2U5t97eki1CnyLij627'}, {'domain': '.aliexpress.com', 'expiry': 1656999699, 'httpOnly': True, 'name': 'xman_t', 'path': '/', 'sameSite': 'None', 'secure': True, 'value': 'M8L1RyHU6YV+srqb2pStsJ1hTpL9EZ48E3Q6y4xI76Fc/Z2lfXctW4oQcwASE6yC'}, {'domain': '.aliexpress.com', 'expiry': 1964583698, 'httpOnly': False, 'name': 'aep_usuc_f', 'path': '/', 'secure': False, 'value': 'site=glo&b_locale=en_US'}]

loginUrl = 'https://login.aliexpress.com'
# 打开登录页
browser.get(loginUrl)
account = '1321345138@qq.com'
password = 'Zmy123'
while 1:
    start = time.clock()
    try:
        browser.find_element_by_id("fm-login-id");
        print('fm-login-id已定位到元素')
        end = time.clock()
        break
    except:
        print("fm-login-id还未定位到元素!")

print('fm-login-id定位耗费时间：' + str(end - start))
time.sleep(1)
loginPage = browser.current_window_handle
accountInput = browser.find_element_by_id('fm-login-id')
accountInput.send_keys(account)
passwordInput = browser.find_element_by_id('fm-login-password')
passwordInput.send_keys(password)
# 点击登录
browser.find_element_by_class_name('login-submit').click()

time.sleep(10)
# #browser.close()#关闭浏览器
print(browser.get_cookies())

# 新开一个窗口，通过执行js来新开一个窗口
# 订单详情页
js = 'window.location.href = "https://gsp.aliexpress.com/apps/order/detail?orderId=5017361981258139&Step=4";'
browser.execute_script(js)

print(browser.current_window_handle)  # 输出当前窗口句柄（百度）
handles = browser.window_handles  # 获取当前窗口句柄集合（列表类型）
print(handles)  # 输出句柄集合
# 切换到订单详情页
#browser.switch_to_window(handles[1])
while 1:
    start = time.clock()
    try:
        browser.find_element_by_xpath('//*[@id="DadaMicroLoaderMicroLoader_1649226607613"]/div/div/div/div/div[3]/div[2]/div/div/div/div/div[3]/div/div[1]/div/div/div[3]/div/div/div/div[2]/a').click()
        print('已定位到元素')
        end = time.clock()
        break
    except:
        print("还未定位到元素!")

print('定位耗费时间：' + str(end - start))

print(browser.current_window_handle)  # 输出当前窗口句柄（百度）
handles = browser.window_handles  # 获取当前窗口句柄集合（列表类型）
print(handles)  # 输出句柄集合
# while 1:
#     start = time.clock()
#     try:
#         browser.find_element_by_id("the-form-message");
#         print('the-form-message已定位到元素')
#         end = time.clock()
#         break
#     except:
#         print("the-form-message还未定位到元素!")
#
# print('the-form-message定位耗费时间：' + str(end - start))
# # 点击第五颗星
# browser.find_element_by_xpath('//*[@id="the-ratings"]/div/div[3]/span[5]').click()
# # 设置评价文本
# browser.find_element_by_id("the-form-message").send_keys("You are a good buyer, looking forward to the next cooperation")
# # 点击评价按钮
# browser.find_element_by_id("feedback-submit-button").click()
