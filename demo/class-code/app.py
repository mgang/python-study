import json

def go2json():
    # 定义结果集
    list = []
    with open('class_code.txt') as f:
        for line in f:
            result = {}
            arr = line.split(" ")
            result['opcode'] = arr[0]
            result['opstr'] = arr[1]
            remark = ''
            for str in arr[2:]:
                remark += str
            result['remark'] = remark.replace("\n","")
            list.append(result)
    text = json.dumps(list, indent=4, ensure_ascii=False)
    with open('class_code.json','w') as j:
        j.write(text)


# 将class_code.txt转化为json格式
if __name__ == '__main__':
    go2json()
    pass
