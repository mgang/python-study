# 操作excel的模块
import xlrd
# 统计分析图形库 pyecharts
from pyecharts.charts import Bar
# 打开excel表格
excelName = "data.xlsx"
workbook = xlrd.open_workbook(excelName)
# 获取第一个sheet
firstSheet = workbook.sheet_by_index(0)
rows = firstSheet.nrows
cols = firstSheet.ncols
print(excelName + "表格的第一个sheet有%d"%rows + "行，%d"%cols + "列")
# 创建一个柱状图对象
bar = Bar()
# 获取第一行
# ['', '衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
row0 = firstSheet.row_values(0)
# 前面多了一个元素，需要删掉
del row0[0]
# 设置x轴
bar.add_xaxis(row0)
# 循环设置y轴数据，从表格第二行开始循环
for i in range(1,rows):
    row_i = firstSheet.row_values(i)
    # 拿到商家的名字å
    sj = row_i[0]
    # 删除行里的第一个元素
    del row_i[0]
    # 设置数组到y轴
    bar.add_yaxis(sj,row_i)
# 渲染到html
bar.render('ds.html')