A_WE = 2 ** 0
A_CS = 2 ** 1

B_WE = 2 ** 2
B_CS = 2 ** 3


ALU_EN = 2 ** 4
# 默认做加法
ALU_SUB = 2 ** 5
ALU_AND = 2 ** 6
ALU_OR = ALU_SUB | ALU_AND
ALU_XOR = 2 ** 7
ALU_NOT = ALU_SUB | ALU_XOR

C_WE = 2 ** 8
C_CS = 2 ** 9

MC_WE = 2 ** 10
MC_CS = 2 ** 11

PC_WE = 2 ** 12
PC_EN = 2 ** 13
PC_CS = 2 ** 14
PC_INC = PC_WE | PC_EN | PC_CS

HLT = 2 ** 15


# (c-(a+b))取反的结果放到内存地址4里
micro = [
    # 内存里1地址的数据放到总线上，并存到A寄存器中,PC计数器加一
    A_WE | A_CS | MC_CS | PC_INC,
    # 将内存里2地址的数据写到B寄存器中
    B_WE | B_CS | MC_CS,
    # A寄存器值加B寄存器值计算加法，将结果存到C寄存器，PC计数器加一
    A_CS | B_CS | ALU_EN | C_CS | C_WE | PC_INC,
    # 将C寄存器里的加法结果写入到B寄存器
    B_WE | B_CS | C_CS,
    # 将内存地址3上的数据存到寄存器A
    A_WE | A_CS | MC_CS,
    # A寄存器值减B寄存器值，将结果存到C寄存器
    A_CS | B_CS | ALU_EN | ALU_SUB | C_CS | C_WE,
    # 将C寄存器里的减法结果写入到A寄存器
    A_WE | A_CS | C_CS,
    # A寄存器值取反，将结果存到C寄存器，PC计数器加一
    A_CS | B_CS | ALU_EN | ALU_NOT | C_CS | C_WE | PC_INC,
    # 将C寄存器里的结果写入到内存地址4中
    MC_WE | MC_CS | C_CS,
    # 停止
    HLT,
]

with open('micro.bin', 'wb') as f:
    for var in micro:
        byte = var.to_bytes(2, byteorder='little')
        f.write(byte)
        print(var, byte)

print('生成成功')

